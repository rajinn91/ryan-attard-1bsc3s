﻿namespace DuplicateFileFindAndDelete
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.SearchButton = new System.Windows.Forms.Button();
            this.FolderListView = new System.Windows.Forms.ListView();
            this.ClearButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.OptionsComboBox = new System.Windows.Forms.ComboBox();
            this.StartButton = new System.Windows.Forms.Button();
            this.FiltersComboBox = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.backgroundWorker = new System.ComponentModel.BackgroundWorker();
            this.bottomWebBrowser = new System.Windows.Forms.WebBrowser();
            this.sideWebBrowser = new System.Windows.Forms.WebBrowser();
            this.paypalWebBrowser = new System.Windows.Forms.WebBrowser();
            this.fbWebBrowser = new System.Windows.Forms.WebBrowser();
            this.SuspendLayout();
            // 
            // folderBrowserDialog
            // 
            this.folderBrowserDialog.Description = "Select a folder from which you want to find and delete duplicate files.";
            this.folderBrowserDialog.RootFolder = System.Environment.SpecialFolder.MyComputer;
            this.folderBrowserDialog.ShowNewFolderButton = false;
            // 
            // SearchButton
            // 
            this.SearchButton.Location = new System.Drawing.Point(13, 13);
            this.SearchButton.Name = "SearchButton";
            this.SearchButton.Size = new System.Drawing.Size(348, 32);
            this.SearchButton.TabIndex = 0;
            this.SearchButton.Text = "Add / Select Folder to Search";
            this.SearchButton.UseVisualStyleBackColor = true;
            this.SearchButton.Click += new System.EventHandler(this.SearchButton_Click);
            // 
            // FolderListView
            // 
            this.FolderListView.Location = new System.Drawing.Point(13, 51);
            this.FolderListView.Name = "FolderListView";
            this.FolderListView.Size = new System.Drawing.Size(348, 315);
            this.FolderListView.TabIndex = 1;
            this.FolderListView.UseCompatibleStateImageBehavior = false;
            // 
            // ClearButton
            // 
            this.ClearButton.Location = new System.Drawing.Point(13, 372);
            this.ClearButton.Name = "ClearButton";
            this.ClearButton.Size = new System.Drawing.Size(348, 32);
            this.ClearButton.TabIndex = 2;
            this.ClearButton.Text = "De-Select All Folders";
            this.ClearButton.UseVisualStyleBackColor = true;
            this.ClearButton.Click += new System.EventHandler(this.ClearButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(367, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 17);
            this.label1.TabIndex = 4;
            this.label1.Text = "File Filters:";
            // 
            // OptionsComboBox
            // 
            this.OptionsComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.OptionsComboBox.Items.AddRange(new object[] {
            "Keep Older File Only",
            "Keep Newer File Only",
            "Create Duplicate File Report - Do Not Delete Anything"});
            this.OptionsComboBox.Location = new System.Drawing.Point(449, 51);
            this.OptionsComboBox.Name = "OptionsComboBox";
            this.OptionsComboBox.Size = new System.Drawing.Size(379, 24);
            this.OptionsComboBox.TabIndex = 5;
            // 
            // StartButton
            // 
            this.StartButton.Location = new System.Drawing.Point(449, 372);
            this.StartButton.Name = "StartButton";
            this.StartButton.Size = new System.Drawing.Size(379, 62);
            this.StartButton.TabIndex = 6;
            this.StartButton.Text = "Go!";
            this.StartButton.UseVisualStyleBackColor = true;
            this.StartButton.Click += new System.EventHandler(this.StartButton_Click);
            // 
            // FiltersComboBox
            // 
            this.FiltersComboBox.FormattingEnabled = true;
            this.FiltersComboBox.Items.AddRange(new object[] {
            "Office Files | *.doc; *.docx; *.xls; *.xlsx; *.ppt; *.pptx",
            "PDF | *.pdf",
            "Images | *.jpg; *.jpeg; *.bmp; *.tif; *.tiff; *.png; *.gif",
            "Video | *.3gp; *.flv; *.avi; *.mov; *.mpg; *.mpeg; *.mp3; *.wmv",
            "Music | *.mp3; *.mp4; *.wav; *.m4a; *.wma;",
            "All Files | *.*"});
            this.FiltersComboBox.Location = new System.Drawing.Point(449, 21);
            this.FiltersComboBox.Name = "FiltersComboBox";
            this.FiltersComboBox.Size = new System.Drawing.Size(379, 24);
            this.FiltersComboBox.TabIndex = 7;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(367, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 17);
            this.label2.TabIndex = 8;
            this.label2.Text = "Options:";
            // 
            // backgroundWorker
            // 
            this.backgroundWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker_DoWork);
            this.backgroundWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker_RunWorkerCompleted);
            // 
            // bottomWebBrowser
            // 
            this.bottomWebBrowser.Location = new System.Drawing.Point(41, 454);
            this.bottomWebBrowser.MinimumSize = new System.Drawing.Size(20, 20);
            this.bottomWebBrowser.Name = "bottomWebBrowser";
            this.bottomWebBrowser.ScriptErrorsSuppressed = true;
            this.bottomWebBrowser.ScrollBarsEnabled = false;
            this.bottomWebBrowser.Size = new System.Drawing.Size(760, 120);
            this.bottomWebBrowser.TabIndex = 9;
            this.bottomWebBrowser.WebBrowserShortcutsEnabled = false;
            // 
            // sideWebBrowser
            // 
            this.sideWebBrowser.Location = new System.Drawing.Point(473, 81);
            this.sideWebBrowser.MinimumSize = new System.Drawing.Size(20, 20);
            this.sideWebBrowser.Name = "sideWebBrowser";
            this.sideWebBrowser.ScriptErrorsSuppressed = true;
            this.sideWebBrowser.ScrollBarsEnabled = false;
            this.sideWebBrowser.Size = new System.Drawing.Size(328, 285);
            this.sideWebBrowser.TabIndex = 10;
            this.sideWebBrowser.WebBrowserShortcutsEnabled = false;
            // 
            // paypalWebBrowser
            // 
            this.paypalWebBrowser.Location = new System.Drawing.Point(41, 401);
            this.paypalWebBrowser.MinimumSize = new System.Drawing.Size(20, 20);
            this.paypalWebBrowser.Name = "paypalWebBrowser";
            this.paypalWebBrowser.ScriptErrorsSuppressed = true;
            this.paypalWebBrowser.ScrollBarsEnabled = false;
            this.paypalWebBrowser.Size = new System.Drawing.Size(320, 52);
            this.paypalWebBrowser.TabIndex = 11;
            this.paypalWebBrowser.WebBrowserShortcutsEnabled = false;
            // 
            // fbWebBrowser
            // 
            this.fbWebBrowser.Location = new System.Drawing.Point(367, 54);
            this.fbWebBrowser.MinimumSize = new System.Drawing.Size(20, 20);
            this.fbWebBrowser.Name = "fbWebBrowser";
            this.fbWebBrowser.ScriptErrorsSuppressed = true;
            this.fbWebBrowser.ScrollBarsEnabled = false;
            this.fbWebBrowser.Size = new System.Drawing.Size(76, 277);
            this.fbWebBrowser.TabIndex = 12;
            this.fbWebBrowser.WebBrowserShortcutsEnabled = false;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 586);
            this.Controls.Add(this.fbWebBrowser);
            this.Controls.Add(this.ClearButton);
            this.Controls.Add(this.paypalWebBrowser);
            this.Controls.Add(this.sideWebBrowser);
            this.Controls.Add(this.bottomWebBrowser);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.FiltersComboBox);
            this.Controls.Add(this.StartButton);
            this.Controls.Add(this.OptionsComboBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.FolderListView);
            this.Controls.Add(this.SearchButton);
            this.Name = "MainForm";
            this.Text = "Duplicate File Find and Delete";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog;
        private System.Windows.Forms.Button SearchButton;
        private System.Windows.Forms.ListView FolderListView;
        private System.Windows.Forms.Button ClearButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox OptionsComboBox;
        private System.Windows.Forms.Button StartButton;
        private System.Windows.Forms.ComboBox FiltersComboBox;
        private System.Windows.Forms.Label label2;
        private System.ComponentModel.BackgroundWorker backgroundWorker;
        private System.Windows.Forms.WebBrowser bottomWebBrowser;
        private System.Windows.Forms.WebBrowser sideWebBrowser;
        private System.Windows.Forms.WebBrowser paypalWebBrowser;
        private System.Windows.Forms.WebBrowser fbWebBrowser;
    }
}

