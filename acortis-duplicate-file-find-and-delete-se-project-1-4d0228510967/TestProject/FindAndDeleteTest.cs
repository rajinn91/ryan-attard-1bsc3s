﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.IO.Log;
using FileFindAndDeleteLibrary.MurmurHash2;
using FileFindAndDeleteLibrary.RadixSort;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FileFindAndDeleteLibrary;

namespace TestProject
{
    
    
    /// <summary>
    ///This is a test class for FindAndDeleteTest and is intended
    ///to contain all FindAndDeleteTest Unit Tests
    ///</summary>
    [TestClass()]
    public class FindAndDeleteTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion

                
        /// <summary>
        ///A test for GetFilesFromDirectory
        ///</summary>
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", "|DataDirectory|\\GetFilesFromDirectory.csv", "GetFilesFromDirectory#csv", DataAccessMethod.Sequential), DeploymentItem("TestProject\\CsvFiles\\GetFilesFromDirectory.csv"), DeploymentItem("FileFindAndDeleteLibrary.dll"), TestMethod()]
        public void GetFilesFromDirectoryTest()
        {
            string expectedException = Convert.ToString(TestContext.DataRow["Exception"]);
            string folder = Convert.ToString(TestContext.DataRow["Folder"]); // Path to folder
            int arraySize = (int)TestContext.DataRow["ArraySize"]; //gets the preset arraysize
            string[] filters = new string[arraySize];
            string[] split = new string[arraySize];
            bool isSplit = false;

            try
            {
                string extensionFilter = Convert.ToString(TestContext.DataRow["Filters"]);
                if (arraySize == 1)
                {
                    filters[0] = "*" + extensionFilter;
                }
                else
                {
                    split = extensionFilter.Split(' ');
                    int splitPos = 0;
                    foreach (string s in split)
                    {
                        filters[splitPos] = "*" + s;
                        splitPos++;
                    }
                    isSplit = true;
                }

                string[] allFiles = Directory.GetFiles(folder, "*", SearchOption.AllDirectories).ToArray<string>();
                List<string> expectedFiles = new List<string>();

                if (isSplit == true)
                {
                    foreach (string s in allFiles)
                    {
                        foreach (string f in split)
                        {
                            if (s.EndsWith(f))
                            {
                                expectedFiles.Add(s);
                            }
                        }
                    }
                }
                else
                {
                    foreach (string s in allFiles)
                    {
                        if (s.EndsWith(extensionFilter))
                        {
                            expectedFiles.Add(s);
                        }
                    }
                }
                expectedFiles.Sort();

                IEnumerable<FileInfo> actual = null;
                actual = FindAndDelete.GetFilesFromDirectory(folder, filters);

                List<string> actualFiles = new List<string>();

                //filling the array with the actual data
                foreach (FileInfo file in actual)
                {
                    actualFiles.Add(file.FullName);
                }
                actualFiles.Sort();

                int result = actualFiles.Except(expectedFiles).Count();

                if (result == 0)
                {
                    result = expectedFiles.Except(actualFiles).Count();
                    if (result == 0)
                    {
                        Assert.AreEqual(expectedFiles.Count, actualFiles.Count);
                    }
                }
                if (!String.IsNullOrWhiteSpace(expectedException))
                {
                    Assert.Fail(expectedException + " was expected, but was not thrown!");
                }
            }
            catch (Exception ex) //the test would still be successful
            {
                if (ex.GetType().Name.Equals(Convert.ToString(TestContext.DataRow["Exception"])))
                {//success

                }
                else
                {//unexpected exception was thrown
                    Assert.Fail("Unexpected: " + ex.GetType().Name + " was thrown.");
                }
            }           
        }

    }
}
