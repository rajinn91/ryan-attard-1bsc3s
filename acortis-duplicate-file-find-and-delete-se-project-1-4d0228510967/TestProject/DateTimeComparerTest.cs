﻿using FileFindAndDeleteLibrary;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace TestProject
{
    
    
    /// <summary>
    ///This is a test class for DateTimeComparerTest and is intended
    ///to contain all DateTimeComparerTest Unit Tests
    ///</summary>
    [TestClass()]
    public class DateTimeComparerTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion




        /// <summary>
        ///A test for Compare.. this always fails because of the bug in the original method
        ///</summary>
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", "|DataDirectory|\\Compare.csv", "Compare#csv", DataAccessMethod.Sequential), DeploymentItem("TestProject\\CsvFiles\\Compare.csv"), TestMethod()]
        public void CompareTest()
        {
            string expectedException = Convert.ToString(TestContext.DataRow["Exception"]);
            DateTimeComparer comparer = new DateTimeComparer();

            int expected = (int)(TestContext.DataRow["Expected"]);
           
                try
                {
                    DateTime x = new DateTime((int)(TestContext.DataRow["YearX"]), (int)(TestContext.DataRow["MonthX"]), (int)(TestContext.DataRow["DayX"]));
                    DateTime y = new DateTime((int)(TestContext.DataRow["YearY"]), (int)(TestContext.DataRow["MonthY"]), (int)(TestContext.DataRow["DayY"]));
                    
                    int actual;
                    actual = comparer.Compare(x, y);
                    Assert.AreEqual(expected, actual);
                    if (!String.IsNullOrWhiteSpace(expectedException))
                    {
                        Assert.Fail(expectedException + " was expected, but was not thrown!");
                    }
                }
                catch (Exception ex) //the test would still be successful
                {
                    if (ex.GetType().Name.Equals(expectedException))
                    {//success
                        
                    }
                    else
                    {//unexpected exception was thrown
                        Assert.Fail("Unexpected exception thrown: "+ex.Message);                        
                    }

                }           
        }

       
    }
}
