﻿using Etier.IconHelper;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections;
using System.Windows.Forms;
using System.Drawing;
using System.IO;

namespace TestProject
{
    
    
    /// <summary>
    ///This is a test class for IconListManagerTest and is intended
    ///to contain all IconListManagerTest Unit Tests
    ///</summary>
    [TestClass()]
    public class IconListManagerTest
    {
        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        private System.Windows.Forms.ImageList imageList = new System.Windows.Forms.ImageList();
        private IconReader.IconSize iconSize = 0; 

        /// <summary>
        ///A test for AddExtension
        ///</summary>
        [DeploymentItem("TestProject\\CsvFiles\\AddExtension.csv"), DeploymentItem("DuplicateFileFindAndDelete.exe"), DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", "|DataDirectory|\\AddExtension.csv", "AddExtension#csv", DataAccessMethod.Sequential), TestMethod()]
        public void AddExtensionTest()
        {
            IconListManager target = new IconListManager(imageList, iconSize);

                string expectedException = Convert.ToString(TestContext.DataRow["Exception"]);
                try
                {
                    
                    string Extension = Convert.ToString(TestContext.DataRow["Extension"]);
                    int ImageListPosition = Convert.ToInt32(TestContext.DataRow["Position"]);

                 
                    //expected
                    Hashtable _extensionExpected = new Hashtable();
                    _extensionExpected.Add(Extension, ImageListPosition);

                    //actual
                    target.AddExtension(Extension, ImageListPosition);

                    Assert.AreEqual((int)_extensionExpected.Count, (int)target._extensionList.Count);

                    if (!String.IsNullOrWhiteSpace(expectedException))
                    {
                        Assert.Fail(expectedException + " was expected, but was not thrown!");
                    }
                }
                catch (Exception ex) //the test would still be successful
                {
                    if (ex.GetType().Name.Equals(expectedException))
                    {//still successful

                    }
                    else
                    {//unexpected exception was thrown
                        Assert.Fail("Unexpected: " + ex.GetType().Name + " was thrown.");
                    }
                }

        }


        [DeploymentItem("TestProject\\CsvFiles\\AddFileIcon.csv"), DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", "|DataDirectory|\\AddFileIcon.csv", "AddFileIcon#csv", DataAccessMethod.Sequential), TestMethod()]
        public void AddFileIconTest()
        {
            string expectedException = Convert.ToString(TestContext.DataRow["Exception"]);

                try
                {                    
                    string path = Convert.ToString(TestContext.DataRow["Path"]);
                    
                    ImageList imageList = new ImageList(); 
                    imageList.ImageSize = new Size(255, 255);
                    imageList.TransparentColor = Color.White;

                    string imageName = Convert.ToString(TestContext.DataRow["ImageName"]);//image name specified in the csv file
                    string imagePath = path + "\\"+imageName;//the path specified in the csv file
                    imageList.Images.Add(Image.FromFile(imagePath));

                    IconReader.IconSize iconSize = IconReader.IconSize.Small; // Initialized as small

                    bool isSmall = Convert.ToBoolean(TestContext.DataRow["Small"]);
                    if(isSmall != true) iconSize = IconReader.IconSize.Large;// if isSmall is false the iconSize would be reset as Large
                    
                    IconListManager target = new IconListManager(imageList, iconSize); 
                    string fileName = Convert.ToString(TestContext.DataRow["FileName"]);

                    string filePath = path + "\\" + fileName;

                    string[] splitPath = filePath.Split(new Char[] { '.' });
                    int expected = (int)(TestContext.DataRow["Expected"]);
                    
                    int actual;
                    actual = target.AddFileIcon(filePath);

                    Assert.AreEqual(expected, actual);

                    if (!String.IsNullOrWhiteSpace(expectedException))
                    {
                        Assert.Fail(expectedException + " was expected, but was not thrown!");
                    }
                }
                catch (Exception ex)
                {
                    if (ex.GetType().Name.Equals(expectedException))
                    {//success
                        
                    }
                    else
                    {
                        Assert.Fail("Unexpected: "+ex.GetType().Name+" was thrown");                        
                    }
                }           
        }
               
    }
}
