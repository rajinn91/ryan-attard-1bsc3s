﻿using Etier.IconHelper;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Drawing;
using System.IO;

namespace TestProject
{
    
    
    /// <summary>
    ///This is a test class for IconReaderTest and is intended
    ///to contain all IconReaderTest Unit Tests
    ///</summary>
    [TestClass()]
    public class IconReaderTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        ///// <summary>
        /////A test for GetFileIcon
        /////</summary>
        //[TestMethod()]
        //public void GetFileIconTest() //when successful
        //{
        //    string name = "C:\\test\\test.pdf"; // path of file 
        //    IconReader.IconSize size = IconReader.IconSize.Large;// setting the iconsize to large
        //    bool linkOverlay = false; // TODO: Initialize to an appropriate value
                        
        //    //temp custom icon             
        //    Icon expected = Icon.ExtractAssociatedIcon(name);
           
            
        //    Icon actual;
        //    actual = IconReader.GetFileIcon(name, size, linkOverlay);

        //    //convert to bitmap
        //    Bitmap actIconBmp = actual.ToBitmap();
        //    Bitmap expIconBmp = expected.ToBitmap();

        //    ImageConverter converter = new ImageConverter();
        //    byte[] rawExpData = converter.ConvertTo(expIconBmp, typeof(byte[])) as byte[];
        //    byte[] rawActData = converter.ConvertTo(actIconBmp, typeof(byte[])) as byte[];

        //    if (rawActData.Length != rawExpData.Length) return;
        //    int count = 0;
        //    int i = 0;
        //    for (i = 0; i < rawActData.Length; i++)
        //    {
        //        if (rawActData[i] != rawExpData[i]) return;
        //        count++;
        //    }

        //    Assert.AreEqual(count, i); //counting the matching values in the byte array   
        //}

        ///// <summary>
        /////A test for GetFolderIcon
        /////</summary>
        //[TestMethod()]
        //public void GetFolderIconTest() //when successful
        //{
        //    //testing an open test folder with small icon
        //    string name = "C:\\test";
        //    IconReader.IconSize size = IconReader.IconSize.Small; // TODO: Initialize to an appropriate value
        //    IconReader.FolderType folderType = IconReader.FolderType.Open; // TODO: Initialize to an appropriate value
            
        //    //temp custom icon
        //    uint flags = Shell32.SHGFI_ICON | Shell32.SHGFI_USEFILEATTRIBUTES;
        //    flags += Shell32.SHGFI_OPENICON;
        //    flags += Shell32.SHGFI_SMALLICON;

        //    Shell32.SHFILEINFO shfi = new Shell32.SHFILEINFO();
        //    IntPtr res = Shell32.SHGetFileInfo(name,
        //                    Shell32.FILE_ATTRIBUTE_DIRECTORY,
        //                    ref shfi,
        //                    (uint)System.Runtime.InteropServices.Marshal.SizeOf(shfi),
        //                    flags);
        //    System.Drawing.Icon.FromHandle(shfi.hIcon);
        //    System.Drawing.Icon icon = (System.Drawing.Icon)System.Drawing.Icon.FromHandle(shfi.hIcon).Clone();
        //    User32.DestroyIcon(shfi.hIcon);
        //    //end of temp icon        

        //    Icon expected = icon; // custom created icon in this method
        //    Icon actual;
        //    actual = IconReader.GetFolderIcon(name, size, folderType); //called the method in the IconReader Class

        //    Bitmap expIconBmp = expected.ToBitmap();
        //    Bitmap actIconBmp = actual.ToBitmap();

        //    ImageConverter converter = new ImageConverter();
        //    byte[] rawExpData = converter.ConvertTo(expIconBmp, typeof(byte[])) as byte[];
        //    byte[] rawActData = converter.ConvertTo(actIconBmp, typeof(byte[])) as byte[];

            
        //    if (rawActData.Length != rawExpData.Length) return;
        //    int count = 0;
        //    int i = 0;
        //    for (i = 0; i < rawActData.Length; i++)
        //    {
        //        if (rawActData[i] != rawExpData[i]) return;
        //        count++;
        //    }

        //    Assert.AreEqual(count,i); //counting the matching values in the byte array   
        //}

       
    }
}
